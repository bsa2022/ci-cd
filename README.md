# Auto API Testing

## Notes:

-   All requests have max execution time of 5s. If your internet is slow, they can fail
-   I used a little bit different architecture than was offered in the 3rd lecture
-   There are many commands that wrap `mocha <something>`, so to avoid endless `--` config type is passed using `TEST_ENV` environment variable. (Using `env` package, this will work on Windows)
-   Instead of helper functions (`expectStatus`, etc.) chai plugin is used (in `tests/plugins/axios`)

## Preconditions:

### Author chain:

-   Author with known credentials exists

### Student chain:

-   Student with known credentials exists
-   At least one course exists
-   At least one weekly goal exists

## Setup:

-   `npm install`

## Test:

-   Development: `npm run test:all:dev`
-   Pre-Production: `npm run test:all:ppr`
-   Above commands will test the app, generate reports in `allure-report` and `junit-report` folders and cleanup results folders
