const supportedEnvNames = ["dev", "ppr"];
const supportedAsString = supportedEnvNames.join(", ");
const defaultEnv = "dev";

let env = process.env.TEST_ENV ?? defaultEnv;

if (!supportedEnvNames.includes(env)) {
    console.error(
        `Unsupported env type: ${env}. Supported: ${supportedAsString}. Using default - ${defaultEnv}`
    );

    env = defaultEnv;
}

module.exports = {
    require: [
        "ts-node/register",
        "tsconfig-paths/register",
        "source-map-support/register",
        `./tests/config/${env}.config.ts`,
        "./tests/setup.ts",
    ],
    timeout: 100000,
    bail: true,
    parallel: false,
    reporter: "mocha-multi-reporters",
    "reporter-options": "configFile=./.reportersrc.json",
};
