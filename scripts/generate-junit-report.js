const fs = require("fs");
const path = require("path");
const { mergeFiles } = require("junit-report-merger");

function getReportId() {
    const date = new Date();
    const year = date.getUTCFullYear();
    const month = String(date.getUTCMonth()).padStart(2, "0");
    const day = String(date.getUTCDate()).padStart(2, "0");
    const hour = String(date.getUTCHours()).padStart(2, "0");
    const minute = String(date.getUTCMinutes()).padStart(2, "0");
    const second = String(date.getUTCSeconds()).padStart(2, "0");

    return `${year}${month}${day}-${hour}${minute}${second}`;
}

const inputFiles = [
    path.join(__dirname, "..", "junit-results", "report-*.xml"),
];

const outputName = `report-${getReportId()}.xml`;
const outputDirectory = path.join(__dirname, "..", "junit-report");
const outputFile = path.join(outputDirectory, outputName);

async function merge() {
    try {
        // There's no promisified alternative of existsSync
        const outputDirExists = fs.existsSync(outputDirectory);

        if (!outputDirExists) {
            await fs.promises.mkdir(outputDirectory);
        }

        await mergeFiles(outputFile, inputFiles);

        console.log(
            `Junit reports are merged, output in junit-report/${outputName}`
        );
    } catch (err) {
        console.error(err);
    }
}

merge();
