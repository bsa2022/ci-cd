global.testsConfig = {
    name: "Development",
    api: {
        baseUrl: "https://knewless.tk/api",
        users: {
            student: {
                valid: {
                    email: "2m.roman2@gmail.com",
                    password: "abc123456",
                },
                invalid: [
                    {
                        email: "2mroman2@gmail.com",
                        password: "abc123456",
                    },
                    {
                        email: "2m.roman2@gmail.com   ",
                        password: "abc123456",
                    },
                    {
                        email: "   2m.roman2@gmail.com",
                        password: "abc123456",
                    },
                    {
                        email: "2m.roman2@gmail.com",
                        password: "1256",
                    },
                    {
                        email: "2m.roman2@gmail.com",
                        password: "abc123456   ",
                    },
                    {
                        email: "2m.roman2@gmail.com",
                        password: "   abc123456",
                    },
                ],
            },
            author: {
                valid: {
                    email: "nirijo2143@5k2u.com",
                    password: "abc123456",
                },
                invalid: [
                    {
                        email: "nirijo@5k2u.com",
                        password: "abc123456",
                    },
                    {
                        email: "   nirijo2143@5k2u.com",
                        password: "abc123456",
                    },
                    {
                        email: "nirijo2143@5k2u.com   ",
                        password: "abc123456",
                    },
                    {
                        email: "nirijo2143@5k2u.com",
                        password: "bc12",
                    },
                    {
                        email: "nirijo2143@5k2u.com",
                        password: "bc12",
                    },
                    {
                        email: "nirijo2143@5k2u.com",
                        password: "bc12",
                    },
                ],
            },
        },
    },
};
