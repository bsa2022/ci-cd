import "axios";

declare module "axios" {
    interface AxiosResponse<T = any, D = any> {
        duration: number;
    }

    interface AxiosRequestConfig<T = any> {
        startTime?: number;
    }
}
