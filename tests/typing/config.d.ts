declare var testsConfig: {
    name: string;
    api: {
        baseUrl: string;
        users: {
            student: {
                valid: LoginModel;
                invalid: LoginModel[];
            };
            author: {
                valid: LoginModel;
                invalid: LoginModel[];
            };
        };
    };
};
