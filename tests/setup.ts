import chai from "chai";
import chaiJsonSchema from "chai-json-schema";
import chaiAxios from "plugins/axios";

console.log(`Using ${testsConfig.name} environment`);

chai.use(chaiJsonSchema);
chai.use(chaiAxios);
