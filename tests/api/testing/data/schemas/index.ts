import articleCommentSchema from "./article-comment.schema.json";
import articleCommentsSchema from "./article-comments.schema.json";
import authorSettingsSchema from "./author-settings.schema.json";
import loginResponseSchema from "./login-response.schema.json";
import meInfoSchema from "./me-info.schema.json";
import overviewSchema from "./author-overview.schema.json";
import articleWithAuthorSchema from "./article-with-author.schema.json";
import articlesSchema from "./articles.schema.json";
import fullArticleSchema from "./full-article.schema.json";
import allCoursesSchema from "./all-courses.schema.json";
import courseCommentSchema from "./course-comment.schema.json";
import courseCommentsSchema from "./course-comments.schema.json";
import goalSchema from "./goal.schema.json";
import goalsSchema from "./goals.schema.json";

export {
    authorSettingsSchema,
    articleCommentSchema,
    articleCommentsSchema,
    loginResponseSchema,
    meInfoSchema,
    overviewSchema,
    articleWithAuthorSchema,
    articlesSchema,
    fullArticleSchema,
    allCoursesSchema,
    courseCommentSchema,
    courseCommentsSchema,
    goalSchema,
    goalsSchema,
};
