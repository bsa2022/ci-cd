import ArticleWithAuthorModel from "api/lib/models/articles/article-with-author.model";
import CreateArticleModel from "api/lib/models/articles/create-article.model";
import FullArticleModel from "api/lib/models/articles/full-article.model";
import AuthorSettingsModel from "api/lib/models/author/author-settings.model";

type DeepPartial<T> = T extends object
    ? { [K in keyof T]?: DeepPartial<T[K]> }
    : T;

interface Options {
    articleId: string;
    articleData: CreateArticleModel;
    userId: string;
    authorId: string;
    authorSettings: AuthorSettingsModel;
    articleList: ArticleWithAuthorModel[];
}

export function fullArticleExpected({
    articleData,
    userId,
    authorId,
    authorSettings: { avatar, firstName, lastName, biography },
    articleList,
}: Options): DeepPartial<FullArticleModel> {
    return {
        id: null, // THIS IS A SERVER BUG
        ...articleData,
        author: {
            id: authorId,
            userId,
            avatar,
            name: firstName + " " + lastName,
            biography,
            articles: articleList,
        },
    };
}
