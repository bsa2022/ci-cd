import CreateArticleModel from "api/lib/models/articles/create-article.model";
import ArticleWithAuthorModel from "api/lib/models/articles/article-with-author.model";
import AuthorSettingsModel from "api/lib/models/author/author-settings.model";

interface Options {
    articleId?: string;
    articleData: CreateArticleModel;
    authorSettings: AuthorSettingsModel;
}

export function articleWithAuthorExpected({
    articleId: id,
    articleData,
    authorSettings: { id: authorId, firstName, lastName },
}: Options): Partial<ArticleWithAuthorModel> {
    return {
        ...(id ? { id } : {}),
        ...articleData,
        authorId,
        authorName: firstName + " " + lastName,
    };
}
