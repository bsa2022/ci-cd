import AuthorOverviewModel from "api/lib/models/author/author-overview.model";
import AuthorSettingsModel from "api/lib/models/author/author-settings.model";

interface Options {
    userId: string;
    authorId: string;
    authorSettings: AuthorSettingsModel;
}

export function overviewExpected({
    userId,
    authorId: id,
    authorSettings: { avatar, firstName, lastName, biography },
}: Options): Partial<AuthorOverviewModel> {
    return {
        id,
        userId,
        avatar,
        firstName,
        lastName,
        biography,
    };
}
