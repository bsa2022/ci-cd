import MeInfoModel from "api/lib/models/user/me-info.model";
import CourseCommentModel from "api/lib/models/course-comment/course-comment.model";

interface Options {
    commentId?: string;
    commentDate?: string;
    commentText: string;
    courseId: string;
    meInfo: MeInfoModel;
}

export function courseCommentExpected({
    commentId: id,
    courseId,
    commentText: text,
    commentDate: date,
    meInfo: { id: userId, email, nickname: username },
}: Options): Partial<CourseCommentModel> {
    return {
        ...(id ? { id } : {}),
        ...(date ? { createdAt: date, updatedAt: date } : {}),
        courseId,
        sourceId: courseId,
        text,
        user: {
            id: userId,
            email,
            username,
            role: "USER",
        },
    };
}
