import FullGoalModel from "api/lib/models/goal/full-goal.model";
import GoalModel from "api/lib/models/goal/goal.model";

interface Options {
    goal: GoalModel;
}

export function goalExpected({
    goal: { id: goalId, name: goalName, durationSeconds: secondsNeededOverall },
}: Options): Partial<FullGoalModel> {
    return {
        goalId,
        goalName,
        secondsNeededOverall,
    };
}
