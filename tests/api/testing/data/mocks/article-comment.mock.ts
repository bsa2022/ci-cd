import { faker } from "@faker-js/faker";
import CreateArticleCommentModel from "api/lib/models/article-comment/create-article-comment.model";

export function articleCommentMock(
    articleId: string
): CreateArticleCommentModel {
    return {
        articleId,
        text: faker.lorem.sentences(5),
    };
}
