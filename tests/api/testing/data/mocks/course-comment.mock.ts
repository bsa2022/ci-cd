import { faker } from "@faker-js/faker";
import CreateCourseCommentModel from "api/lib/models/course-comment/create-course-comment.model";

export function courseCommentMock(courseId: string): CreateCourseCommentModel {
    return {
        courseId,
        text: faker.lorem.sentences(5),
    };
}
