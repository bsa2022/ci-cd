import AuthorSettingsModel from "api/lib/models/author/author-settings.model";
import { faker } from "@faker-js/faker";

export function authorSettingsMock(authorId: string): AuthorSettingsModel {
    return {
        id: authorId,
        avatar: faker.image.avatar(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        company: faker.company.companyName(),
        job: faker.name.jobTitle(),
        website: faker.internet.url(),
        twitter: `https://twitter.com/${faker.internet.userName()}`,
        location: faker.address.country(),
        biography: faker.lorem.paragraphs(5),
    };
}
