import { faker } from "@faker-js/faker";
import CreateArticleModel from "api/lib/models/articles/create-article.model";

export function articleMock(): CreateArticleModel {
    return {
        name: faker.lorem.words(3),
        text: faker.lorem.paragraphs(50),
        image: faker.image.abstract(),
    };
}
