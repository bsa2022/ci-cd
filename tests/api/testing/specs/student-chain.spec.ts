import { expect } from "chai";
import HttpHelper from "api/lib/helpers/http.helper";
import ArrayHelper from "api/lib/helpers/array.helper";
import IDHelper from "api/lib/helpers/id.helper";
import ShortCourseModel from "api/lib/models/courses/short-course.model";
import MeInfoModel from "api/lib/models/user/me-info.model";
import GoalModel from "api/lib/models/goal/goal.model";
import useUnauthorized from "../../../helpers/session.helper";
import { useTestFact, useTestTheory } from "../../../helpers/test-data.helper";

import {
    AuthService,
    CourseService,
    UserService,
    CourseCommentService,
    GoalService,
    StudentService,
} from "api/lib/services";

import { courseCommentMock } from "../data/mocks";

import {
    allCoursesSchema,
    courseCommentSchema,
    courseCommentsSchema,
    goalSchema,
    goalsSchema,
    loginResponseSchema,
    meInfoSchema,
} from "../data/schemas";

import {
    courseCommentExpected,
    meInfoExpected,
    goalExpected,
} from "../data/student-chain/expected";

const { valid: loginData, invalid: invalidLoginData } =
    testsConfig.api.users.student;

const authService = new AuthService();
const userService = new UserService();
const courseService = new CourseService();
const courseCommentService = new CourseCommentService();
const goalService = new GoalService();
const studentService = new StudentService();

describe("Student chain", () => {
    let meInfo: MeInfoModel;
    let courseId: string;
    let unexistingCourseId: string;
    let course: ShortCourseModel;
    let commentId: string;
    let commentText: string;
    let commentDate: string;
    let goal: GoalModel;

    before(() => HttpHelper.addAndEnterSession("default"));
    after(() => HttpHelper.exitAndRemoveSession());

    useTestFact(
        [
            { loginData, valid: true },
            ...invalidLoginData.map((loginData) => ({
                loginData,
                valid: false,
            })),
        ],
        ({ loginData, valid }) => {
            const expectedStatus = valid ? 200 : 401;

            const expectedAction = valid
                ? "log in successfully"
                : "reject log in";

            const name = `should ${expectedAction} with credentials: email - ${loginData.email}, password - ${loginData.password}`;
            const hook = valid ? before : it;

            hook(name, async () => {
                const response = await authService.login(loginData);

                expect(response).to.have.normalExecutionTime;
                expect(response).to.have.status(expectedStatus);

                if (valid) {
                    expect(response.data).to.have.jsonSchema(
                        loginResponseSchema
                    );

                    HttpHelper.setAuthtoken(response.data.accessToken);
                }
            });
        }
    );

    it("should get correct current user info", async () => {
        const response = await userService.getMe();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(meInfoSchema);
        expect(response.data).to.deep.include(meInfoExpected({ loginData }));

        meInfo = response.data;
    });

    it("should get all courses", async () => {
        const response = await courseService.getAllCourses();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(allCoursesSchema);

        const courseIds = (response.data as ShortCourseModel[]).map(
            ({ id }) => id
        );

        course = ArrayHelper.choose(response.data);
        courseId = course.id;
        unexistingCourseId = IDHelper.getUnexistingId(courseIds);
    });

    useTestTheory(
        [
            [{ existing: false }, () => ({ courseId: unexistingCourseId })],
            [{ existing: true }, () => ({ courseId })],
        ],
        (data) => {
            const { existing } = data;

            const name = existing
                ? "should create comment for course"
                : "should reject to create comment for unexisting course";

            const expectedStatus = existing ? 200 : 404;

            it(name, async () => {
                const { courseId } = data;
                const commentData = courseCommentMock(courseId);

                const response = await courseCommentService.createComment(
                    commentData
                );

                expect(response).to.have.normalExecutionTime;
                expect(response).to.have.status(expectedStatus);

                if (existing) {
                    commentText = commentData.text;

                    expect(response.data).to.have.jsonSchema(
                        courseCommentSchema
                    );

                    expect(response.data).to.deep.include(
                        courseCommentExpected({
                            courseId,
                            commentText,
                            meInfo,
                        })
                    );

                    commentId = response.data.id;
                    commentDate = response.data.createdAt;
                }
            });
        }
    );

    it("should get comments for course", async () => {
        const response = await courseCommentService.getForCourse(courseId);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(courseCommentsSchema);

        expect(response.data).to.deep.include(
            courseCommentExpected({
                commentId,
                courseId,
                commentText,
                commentDate,
                meInfo,
            })
        );
    });

    it("should get weekly goals", async () => {
        const response = await goalService.getAll();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(goalsSchema);

        goal = ArrayHelper.choose(response.data);
    });

    useTestFact(
        [{ authorized: false }, { authorized: true }],
        ({ authorized }) => {
            const name = authorized
                ? "should set weekly goal"
                : "should reject to set weekly goal for unauthorized user";

            const expectedStatus = authorized ? 200 : 401;

            it(name, async () => {
                const doRequest = () => studentService.setGoal(goal.id);

                const response = await (authorized
                    ? doRequest()
                    : useUnauthorized(doRequest));

                expect(response).to.have.normalExecutionTime;
                expect(response).to.have.status(expectedStatus);
            });
        }
    );

    it("should get correct weekly goal", async () => {
        const response = await studentService.getGoal();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(goalSchema);
        expect(response.data).to.deep.include(goalExpected({ goal }));
    });
});
