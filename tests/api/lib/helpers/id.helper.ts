import { v4 as uuidv4 } from "uuid";

export default class IDHelper {
    public static getUnexistingId(existing: string[]): string {
        let id: string = uuidv4();

        while (existing.includes(id)) {
            id = uuidv4();
        }

        return id;
    }
}
