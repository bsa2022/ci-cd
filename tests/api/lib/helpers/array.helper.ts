export default class ArrayHelper {
    public static choose<T = any>(array: T[]): T {
        return array[Math.floor(Math.random() * array.length)] as T;
    }
}
