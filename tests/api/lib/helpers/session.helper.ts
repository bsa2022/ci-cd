export default class Session<T extends object> {
    public constructor(protected readonly data: T) {}

    public get(key: keyof T): T[typeof key] {
        return this.data[key];
    }

    public set(key: keyof T, value: T[typeof key]) {
        this.data[key] = value;
    }
}
