export default interface FullGoalModel {
    goalId: string;
    goalName: string;
    goalStarted: string;
    goalExpires: string;
    done: boolean;
    congratulationShown: boolean;
    percentsDone: number;
    secondsDone: number;
    secondsLeft: number;
    secondsNeededOverall: number;
}
