export default interface GoalModel {
    id: string;
    name: string;
    durationSeconds: number;
}
