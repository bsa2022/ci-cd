import ArticleWithAuthorModel from "../articles/article-with-author.model";

export interface AuthorWithArticlesModel {
    id: string;
    userId: string;
    avatar: string | null;
    name: string;
    biography: string;
    articles: ArticleWithAuthorModel[];
}
