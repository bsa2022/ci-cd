export default interface ArticleModel {
    id: string;
    name: string;
    text: string;
    image: string;
}
