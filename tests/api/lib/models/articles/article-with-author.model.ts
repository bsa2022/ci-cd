export default interface ArticleWithAuthorModel {
    id: string;
    name: string;
    text: string;
    image: string;
    authorId: string;
    authorName: string;
}
