import { AuthorWithArticlesModel } from "../author/author-with-articles.model";

export default interface FullArticleModel {
    id: string | null;
    name: string;
    text: string;
    image: string;
    favourite: boolean;
    author: AuthorWithArticlesModel;
}
