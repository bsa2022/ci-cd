export default interface CreateArticleCommentModel {
    text: string;
    articleId: string;
}
