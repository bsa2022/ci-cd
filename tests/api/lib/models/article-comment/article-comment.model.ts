import ShortUserInfo from "../user/short-user.model";

export default interface ArticleCommentModel {
    id: string;
    text: string;
    articleId: string;
    sourceId: string;
    user: ShortUserInfo;
    createdAt: string;
    updatedAt: string;
}
