export default interface TagModel {
    id: string;
    name: string;
    imageSrc: string;
}
