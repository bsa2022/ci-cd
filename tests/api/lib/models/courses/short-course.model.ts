export default interface ShortCourseModel {
    id: string;
    imageSrc: string;
    name: string;
    description: string;
    duration: number;
    lectures: number;
    members: number;
    rating: number;
    ratingCount: number;
    releasedDate: string;
    level: "BEGINNER" | "INTERMEDIATE" | "ADVANCED";
    tags: string[];
    authorId: string;
    authorName: string;
}
