import TagModel from "../tags/tag.model";

export default interface AuthorCourseModel {
    id: string;
    name: string;
    description: string;
    imageSrc: string;
    author: string;
    allReactions: number;
    duration: number;
    lectures: number;
    members: number;
    positiveReactions: number;
    level: "BEGINNER" | "INTERMEDIATE" | "ADVANCED";
    tags: TagModel[];
    updatedAt: string;
}
