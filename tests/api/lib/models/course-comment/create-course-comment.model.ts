export default interface CreateCourseCommentModel {
    text: string;
    courseId: string;
}
