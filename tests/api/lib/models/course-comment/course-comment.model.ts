import ShortUserInfo from "../user/short-user.model";

export default interface CourseCommentModel {
    id: string;
    text: string;
    courseId: string;
    sourceId: string;
    user: ShortUserInfo;
    createdAt: string;
    updatedAt: string;
}
