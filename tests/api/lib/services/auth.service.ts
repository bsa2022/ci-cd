import LoginModel from "../models/auth/login.model";
import AbstractService from "./abstract.service";

export class AuthService extends AbstractService {
    public constructor() {
        super("/auth");
    }

    public login(data: LoginModel) {
        return this.httpHelper.post("/login", data);
    }
}
