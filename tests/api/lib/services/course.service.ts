import AbstractService from "./abstract.service";

export class CourseService extends AbstractService {
    public constructor() {
        super("/course");
    }

    public getAllCourses() {
        return this.httpHelper.get("/all");
    }
}
