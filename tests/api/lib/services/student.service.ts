import AbstractService from "./abstract.service";

export class StudentService extends AbstractService {
    public constructor() {
        super("/student");
    }

    public setGoal(goalId: string) {
        return this.httpHelper.post("/goal", { goalId });
    }

    public getGoal() {
        return this.httpHelper.get("/goal");
    }
}
