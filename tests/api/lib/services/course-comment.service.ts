import CreateCourseCommentModel from "../models/course-comment/create-course-comment.model";
import AbstractService from "./abstract.service";

export class CourseCommentService extends AbstractService {
    public constructor() {
        super("/course_comment");
    }

    public createComment(data: CreateCourseCommentModel) {
        return this.httpHelper.post("", data);
    }

    public getForCourse(courseId: string) {
        return this.httpHelper.get(`/of/${courseId}`, {
            params: { size: 200 },
        });
    }
}
