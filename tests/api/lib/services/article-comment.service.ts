import CreateArticleCommentModel from "../models/article-comment/create-article-comment.model";
import AbstractService from "./abstract.service";

export class ArticleCommentService extends AbstractService {
    public constructor() {
        super("/article_comment");
    }

    public createComment(data: CreateArticleCommentModel) {
        return this.httpHelper.post("", data);
    }

    public getForArticle(articleId: string) {
        return this.httpHelper.get(`/of/${articleId}`, {
            params: { size: 200 },
        });
    }
}
