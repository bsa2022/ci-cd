import AbstractService from "./abstract.service";

export class UserService extends AbstractService {
    public constructor() {
        super("/user");
    }

    public getMe() {
        return this.httpHelper.get("/me");
    }
}
