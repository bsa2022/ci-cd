export function useTestFact<T>(dataSet: T[], createCase: (data: T) => void) {
    dataSet.forEach(createCase);
}

export function useTestTheory<
    S extends Record<string | symbol, any>,
    D extends Record<string | symbol, any>
>(dataSet: Array<[S, () => D] | (() => D)>, createCase: (data: S & D) => void) {
    dataSet.forEach((data) => {
        if (Array.isArray(data)) {
            const [strict, dynamic] = data;

            const proxy = new Proxy(
                {},
                {
                    get: (_, propertyKey) => {
                        if (strict.hasOwnProperty(propertyKey)) {
                            return strict[propertyKey];
                        }

                        return dynamic()[propertyKey];
                    },
                }
            );

            return createCase(proxy as S & D);
        }

        const proxy = new Proxy(
            {},
            {
                get: (_, propertyKey) => {
                    return data()[propertyKey];
                },
            }
        );

        createCase(proxy as S & D);
    });
}
