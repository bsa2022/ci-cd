import HttpHelper from "api/lib/helpers/http.helper";

export default async function useUnauthorized(func: () => any | Promise<any>) {
    HttpHelper.addAndEnterSession("unauthorized");
    const result = await func();
    HttpHelper.switchSession("default");
    HttpHelper.removeSession("unauthorized");

    return result;
}
