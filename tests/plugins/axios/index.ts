import { AxiosResponse } from "axios";
import AxiosPluginConstants from "./constants";

export default function chaiAxios(
    chai: Chai.ChaiStatic,
    _utils: Chai.ChaiUtils
) {
    chai.Assertion.addProperty("normalExecutionTime", function () {
        const response = this._obj as AxiosResponse;

        if (!response.hasOwnProperty("duration")) {
            throw new Error("Response must have a duration property");
        }

        this.assert(
            response.duration <= AxiosPluginConstants.NormalExecutionTimeMs,
            "Expected response to have execution time less or equal to #{exp}, but got #{act}",
            "Expected response to have execution time less or equal to #{exp}, but got greater",
            AxiosPluginConstants.NormalExecutionTimeMs,
            response.duration
        );
    });

    chai.Assertion.addMethod("status", function (status: number) {
        const response = this._obj as AxiosResponse;

        if (!response.hasOwnProperty("status")) {
            throw new Error("Response must have a status property");
        }

        this.assert(
            response.status === status,
            "Expected response to have status #{exp}, but got #{act}",
            "Expected response to have status #{exp}, but got different",
            status,
            response.status
        );
    });
}
